from src.json.jsondb import Websites, Website
from src.ciphers.AES import AES_Encrypt, AES_Decrypt
from re import sub
from Crypto.Cipher import AES


def convert(s):
    new = ""
    for x in s:
        new += x
        if x == '}':
            new += ','
    return new


def main():
    websites = Websites()
    websites.add_website(Website("google", "username", "password"))
    websites.add_website(Website("yahoo", "username", "password"))
    websites.add_website(Website("bing", "username", "password"))
    cipher = AES.new('passphrase'.ljust(32, ' '))

    jsontext = websites.jsonify()
    print('current database contents: {}'.format(jsontext))

    encryptedjson = AES_Encrypt(jsontext, cipher)
    print('encrypted database contents: {}'.format(encryptedjson))

    filename = 'testfilename.txt'
    print('writing to file {}'.format(filename))

    f = open(filename, 'wb')
    f.write(encryptedjson)
    f.close()

    f = open(filename, 'rb')
    content = f.read()
    f.close()
    print('file contents {}'.format(content))

    decryptedjson = AES_Decrypt(content, cipher)
    print('decrypted json: {}'.format(decryptedjson))
    converted = sub('\" \"', '\", \"', convert(decryptedjson))[:-2] + ']'
    print('Converted: {}'.format(converted))
    website2 = Websites()
    website2.load_json(converted)
    website2.print()



if __name__ == '__main__':
    main()

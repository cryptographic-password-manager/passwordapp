import http.server
import socketserver
import getpass
from src.ciphers import *
from src.ciphers.AES import *
from urllib.parse import unquote

PORT = 8000

class Handler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path != "/":
            self.send_response(404)
            # content = "not found"
            # self.send_header("Content-Type", "text/plain")
            # self.send_header("Content-Length", str(len(content)))
            # self.end_headers()
            # self.wfile.write(content.encode("utf-8"))
            return
        indexPage = open("src/index.html", "r")
        content = indexPage.read()
        indexPage.close()
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Length", str(len(content)))
        self.end_headers()
        self.wfile.write(content.encode("ascii"))
        return Handler

    def do_POST(self):
        length = int(self.headers.get_all('Content-Length')[0])
        unparsed = self.rfile.read(length)
        unparsed = unparsed.decode('UTF-8')
        halfParsed = unparsed.split('&')
        fullParsed = dict(line.split('=') for line in halfParsed)
        
        key = fullParsed['key'].ljust(32, ' ')
        cipher = AES.AES.new(key)
        
        testInstance = controller.CipherController
        if fullParsed['direction'] == 'encrypt':
            print("Encrypting package")
            testInstance.memory = [fullParsed['data']]
            result = controller.CipherController.encrypt_menu_logic(testInstance, '2', fullParsed['key'])
            content = str(result)
        
        if fullParsed['direction'] == 'decrypt':
            print("Decrypting package")
            fullParsed['data'] = unquote(fullParsed['data'])

            testVar2 = bytes(fullParsed['data'][1:].replace("'", ""), 'utf-8')
            
            testVar3 = testVar2.decode('unicode-escape').encode('ISO-8859-1')
            testInstance.memory = testVar3
            # testInstance.memory = fullParsed['data'].encode("utf-16-be")
            result = controller.CipherController.decrypt_menu_logic(testInstance, '2', fullParsed['key'])
            content = str(result)

        # content = 'thanks for the data'
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Length", str(len(content)))
        self.end_headers()
        self.wfile.write(content.encode("ascii"))
        return Handler 

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
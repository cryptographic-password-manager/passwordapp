/* This file contains all unit tests for DES encryption */

//Test Arrays:

//64bit initiating key for testing purposes
char key64bitTEST[] = {
	0,0,0,1, 0,0,1,1, 0,0,1,1, 0,1,0,0, 
	0,1,0,1, 0,1,1,1, 0,1,1,1, 1,0,0,1, 
	1,0,0,1, 1,0,1,1, 1,0,1,1, 1,1,0,0, 
	1,1,0,1, 1,1,1,1, 1,1,1,1, 0,0,0,1
};
// Backup of the 64bit key used when resetting the data
char key64bitBACKUP[] = {
	0,0,0,1, 0,0,1,1, 0,0,1,1, 0,1,0,0, 
	0,1,0,1, 0,1,1,1, 0,1,1,1, 1,0,0,1, 
	1,0,0,1, 1,0,1,1, 1,0,1,1, 1,1,0,0, 
	1,1,0,1, 1,1,1,1, 1,1,1,1, 0,0,0,1
};
array_t key64bitTEST_t;

//The message to be encrypted during testing
char messageTEST[] = {
	0,0,0,0, 0,0,0,1, 0,0,1,0, 0,0,1,1,
	0,1,0,0, 0,1,0,1, 0,1,1,0, 0,1,1,1,
	1,0,0,0, 1,0,0,1, 1,0,1,0, 1,0,1,1,
	1,1,0,0, 1,1,0,1, 1,1,1,0, 1,1,1,1
};
array_t messageTEST_t;

//Backup of the message used when resetting the data
char messageBACKUP[] = {
	0,0,0,0, 0,0,0,1, 0,0,1,0, 0,0,1,1,
	0,1,0,0, 0,1,0,1, 0,1,1,0, 0,1,1,1,
	1,0,0,0, 1,0,0,1, 1,0,1,0, 1,0,1,1,
	1,1,0,0, 1,1,0,1, 1,1,1,0, 1,1,1,1
};
array_t messageBACKUP_t;

// This is the result of a correct permutation using PC1 and the 64bit key defined above
char PC1TestKey[] = {
	1,1,1,1,0,0,0, 0,1,1,0,0,1,1, 
	0,0,1,0,1,0,1, 0,1,0,1,1,1,1, 
	0,1,0,1,0,1,0, 1,0,1,1,0,0,1, 
	1,0,0,1,1,1,1, 0,0,0,1,1,1,1
};
array_t PC1TestKey_t;

// Declare Test Functions
void testNewArray();
void testPermutate();
void testSplitInTwo();
void testCreateKeys();
void testExpand32to48();
void testXOR();
void testSBox();
void testFunctionF();
void testEncrypt();
void resetData();


// Start Test Function
void startTests() {
	// Load test arrays
	messageTEST_t = assignArray(INITIALMESSAGESIZE, messageTEST);
	key64bitTEST_t = assignArray(INITIALKEYSIZE, key64bitTEST);
	PC1TestKey_t = assignArray(SUBKEYMAXLENGTH, PC1TestKey);

	// Run tests
	printf("Starting Unit Tests\n");

	printf("Testing newArray() Function\n");
	testNewArray();
	resetData();

	printf("Testing Permutate() Function...\n");
	testPermutate();
	resetData();

	printf("Testing createKeys() Function\n");
	testCreateKeys();
	resetData();

	printf("Testing expand32to48() Function\n");
	testExpand32to48();
	resetData();

	printf("Testing XOR() Function\n");
	testXOR();
	resetData();

	printf("Testing sBox() Function\n");
	testSBox();
	resetData();

	printf("Testing functionF()\n");
	testFunctionF();
	resetData();

	printf("Testing encrypt()\n");
	testEncrypt();
	resetData();

	printf("All tests passed! You are awesome!\n");
}

// Tests the assignArray function
void testNewArray(){
	
	char array[] = {1,0,1,0};
	int arrayLength = 4;
	int i;
	array_t testArray;

	//Run the assignArray function
	testArray = assignArray(arrayLength, array);

	//Assert the correct values are returned
	assert(testArray.length == arrayLength);
	for (i=0; i<arrayLength; i++) {
		assert(testArray.data[i] == array[i]);
	}
}

// Tests the permutate() function
void testPermutate(){

	int i;
	int keySize = PC1_t.length;

	//Run the permutate function
	permutate(key64bitTEST_t, PC1_t);

	//Assert the correct values are returned
	for (i=0; i < keySize; i++){
		assert(PC1TestKey_t.data[i] == key64bitTEST_t.data[i]);
	};
}

// Tests createKeys() function
void testCreateKeys(){

	//Run the create keys function
	createKeys(key64bitTEST_t);
	int i;

	// The final key that should have been created
	char finalKey1[] = {
		0,0,0,1,1,0, 1,1,0,0,0,0,
		0,0,1,0,1,1, 1,0,1,1,1,1,
		1,1,1,1,1,1, 0,0,0,1,1,1,
		0,0,0,0,0,1, 1,1,0,0,1,0
	};

	//Assert the correct values are returned
	for (i=0; i < 48; i++) {
		assert(subKeys_t[1].data[i] == finalKey1[i]);
	};
	//Ensure the length has been set correctly
	assert(subKeys_t[1].length == SUBKEYLENGTH);
}

//Tests the expand function
void testExpand32to48() {

	int i;

	// A 32bit data array for testing purposes.
	char testInput[48] = {
		1,1,1,1, 0,0,0,0, 
		1,0,1,0, 1,0,1,0, 
		1,1,1,1, 0,0,0,0, 
		1,0,1,0, 1,0,1,0
	};
	array_t testExpandInput_t = assignArray(32, testInput);

	//Run the expand function
	expand32to48(testExpandInput_t, expansionTable_t);

	// The correct expansion of the above testing array.
	char testOutput[] = {
	0,1,1,1,1,0, 1,0,0,0,0,1, 
	0,1,0,1,0,1, 0,1,0,1,0,1, 
	0,1,1,1,1,0, 1,0,0,0,0,1, 
	0,1,0,1,0,1, 0,1,0,1,0,1
	};

	//Assert the correct values are returned
	for (i=0; i < 48; i++) {
	assert(testExpandInput_t.data[i] == testOutput[i]);
	};

}

//Tests the XOR function
void testXOR(){
	
	//All possible cases tested
	assert(XOR(1,1) == 0);
	assert(XOR(1,0) == 1);
	assert(XOR(0,1) == 1);
	assert(XOR(0,0) == 0);
}

//Tests the sBox function
void testSBox(){

	int i;

	char testInput[] = {
		0,1,1,0,0,0, 0,1,0,0,0,1, 
		0,1,1,1,1,0, 1,1,1,0,1,0, 
		1,0,0,0,0,1, 1,0,0,1,1,0, 
		0,1,0,1,0,0, 1,0,0,1,1,1
	};
	array_t testInput_t = assignArray(48, testInput);

	char testOutput[] = {
		0,1,0,1, 1,1,0,0, 
		1,0,0,0, 0,0,1,0, 
		1,0,1,1, 0,1,0,1, 
		1,0,0,1, 0,1,1,1
	};

	//Run the sBox function
	sBox(testInput_t);

	//Assert the correct values are returned
	for (i=0; i < 32; i++) {
	assert(testInput_t.data[i] == testOutput[i]);
	};
}

//Tests the F Function
void testFunctionF(){

	int i;
	int testKeyNum = 1;


	char testInput[] = {
		1,1,0,0, 1,1,0,0, 0,0,0,0, 0,0,0,0, 
		1,1,0,0, 1,1,0,0, 1,1,1,1, 1,1,1,1,
		1,1,1,1, 0,0,0,0, 1,0,1,0, 1,0,1,0,
		1,1,1,1, 0,0,0,0, 1,0,1,0, 1,0,1,0
	};
	array_t testInput_t = assignArray(64, testInput);

	//Ensure the keys are generated
	createKeys(key64bitTEST_t);

	char testOutput[] = {
		1,1,1,1, 0,0,0,0, 1,0,1,0, 1,0,1,0,
		1,1,1,1, 0,0,0,0, 1,0,1,0, 1,0,1,0,
		1,1,1,0, 1,1,1,1, 0,1,0,0, 1,0,1,0, 
		0,1,1,0, 0,1,0,1, 0,1,0,0, 0,1,0,0,
	};

	//Run the F Function
	functionF(testInput_t, testKeyNum, TRUE);

	//Assert the correct values are returned
	for (i=0; i < 64; i++) {
		assert(testInput_t.data[i] == testOutput[i]);
	};
};

//Test the encrypt function
void testEncrypt() {

	char correctEncryption[] = {
		1,0,0,0,0,1,0,1, 1,1,1,0,1,0,0,0,
		0,0,0,1,0,0,1,1, 0,1,0,1,0,1,0,0,
		0,0,0,0,1,1,1,1, 0,0,0,0,1,0,1,0, 
		1,0,1,1,0,1,0,0, 0,0,0,0,0,1,0,1
	};

	//Ensure the keys are generated
	createKeys(key64bitTEST_t);

	//Run the encrypt function
	encrypt(messageTEST_t);

	int i;

	//Assert the correct values are returned
	for (i=0; i < 64; i++) {
		assert(messageTEST_t.data[i] == correctEncryption[i]);
	};

}

//This function resets the data that may have been modified in a previous test
void resetData(){

	//Copy the data across
	memcpy(key64bitTEST, key64bitBACKUP, INITIALKEYSIZE);
	memcpy(messageTEST, messageBACKUP, INITIALMESSAGESIZE);

	//Assign the data back to the proper arrays
	messageTEST_t = assignArray(64, messageTEST);
	key64bitTEST_t = assignArray(64, key64bitTEST);
}
// des_functions.c
// This file includes the functions for running the DES algorithm

//Declaration of Functions
array_t assignArray (int length, char *array);
void loadArrays();
void createKeys(array_t key64bit_t);
void permutate(array_t binaryArray_t, array_t permutateTable_t);
void permutateSubKey(int subKeyNum, array_t permutateTable_t);
void expand32to48(array_t binaryArray_t, array_t expandTable_t);
char XOR (char a, char b);
void sBox (array_t binaryArray_t);
void functionF (array_t dataBlock_t, int keyNum, char reverse);
void encrypt (array_t dataBlock_t);

/*
  This function loads the required data into an array structure
  
  INPUTS: length of array (int), pointer to the array to assign
  OUTPUTS: Address (pointer) to the new array structure
*/

array_t assignArray (int length, char *array){

    array_t newArray;
    newArray.length = length;
    newArray.data = array;
    return newArray;
}

/*
	This function loads all of the pre-defined arrays in the header file into an array_t
		type structure so that the length of the array can be stored

	INPUTS: None
	OUTPUTS: None

	NOTE: There is no unit test for this function
*/
void loadArrays() {
	IP_t = assignArray(INITIALKEYSIZE, IP);
	PC1_t = assignArray(PC1LENGTH, PC1);
	PC2_t = assignArray(PC2LENGTH, PC2);
	expansionTable_t = assignArray(EXPANSIONTABLESIZE, expansionTable);
	P_t = assignArray(FUNCTION_PERMUTATION_SIZE, P);
	FP_t = assignArray(INITIALMESSAGESIZE, FP);
}

/*
	This function creates the specified number of keys based off of an initial key

	INPUTS: 64bit key (array_t)
	OUTPUTS: NONE - it modifies a previously declared array of keys in the header file
*/
void createKeys(array_t key64bit_t){

	int i, keyNum, pos;
	int length = SUBKEYMAXLENGTH; // eg. 56
	int halfLength = (length / 2); // eg. 28
	int numShifts;
	int firstKeyBitLeft, firstKeyBitRight;

	// Step 1 - Permutate the Key (becomes 56bit key)
	permutate(key64bit_t, PC1_t);

	// Step 2 - Populate the initial subkey
	for (i=0; i < halfLength; i++) {
		subKeys_t[0].data[i] = key64bit_t.data[i];
		subKeys_t[0].data[i + halfLength] = key64bit_t.data[i + halfLength];
	}
	subKeys_t[0].length = length;

	// Step 3 - Populate the 16 subkeys using pre-defined left shifts
	for (keyNum=1; keyNum < NUMSUBKEYS; keyNum++) {
		for (numShifts=0; numShifts < SHIFTS[keyNum-1]; numShifts++) {
			firstKeyBitLeft = subKeys_t[keyNum-1].data[0 + numShifts];
			firstKeyBitRight = subKeys_t[keyNum-1].data[halfLength + numShifts];
			for (pos=0; pos < halfLength; pos++) {
				subKeys_t[keyNum].data[pos] = subKeys_t[keyNum - 1 + numShifts].data[pos+1];
				subKeys_t[keyNum].data[pos + halfLength] = subKeys_t[keyNum - 1 + numShifts].data[pos + halfLength + 1];
			}
			subKeys_t[keyNum].data[halfLength-1] = firstKeyBitLeft;
			subKeys_t[keyNum].data[length-1] = firstKeyBitRight;
		}
		subKeys_t[keyNum].length = length;
	}

	//Step 4 - Run the final permutation on each key, turning them into 48bit keys
	for (keyNum = 1; keyNum < NUMSUBKEYS; keyNum++) {
		permutateSubKey(keyNum, PC2_t);
	}
}

/*
	This function permutates an input datablock of type array_t

	INPUTS: datablock (array_t), permutate table (array_t)
	OUTPUTS: NONE - it modifies the input data block
*/
void permutate(array_t binaryArray_t, array_t permutateTable_t){

	int i;
	char tempArray[permutateTable_t.length];

	// Append the new order of bits to a temporary array
	for (i=0; i < permutateTable_t.length; i++) {
		// Subtract 1 since the permitation table starts at 1, not 0.
		tempArray[i] = binaryArray_t.data[(int)permutateTable_t.data[i] - 1];
	}

	// Copy this temporary data back to the original array
	for (i=0; i < permutateTable_t.length; i++) {
		binaryArray_t.data[i] = tempArray[i];
	}
}

/*
	This function permutates a subKey_t with an input permutation table

	INPUTS: sub key number, permutation array to permutate with
	OUTPUTS: NONE - it modifies the input data block

	NOTE: There is no unit test for this function as it is very similar to the above
			permutate function
			This function also relies upon the createKeys() function having been run

*/
void permutateSubKey(int subKeyNum, array_t permutateTable_t){

	int i;
	char tempArray[permutateTable_t.length];

	// Append the new order of bits to a temporary array
	for (i=0; i < permutateTable_t.length; i++) {
		// Subtract 1 since the permitation table starts at 1, not 0
		tempArray[i] = subKeys_t[subKeyNum].data[(int)permutateTable_t.data[i] - 1];
	}

	// Copy this temporary data back to the original array
	for (i=0; i < permutateTable_t.length; i++) {
		subKeys_t[subKeyNum].data[i] = tempArray[i];
	}

	// Append the length to the subkey
	subKeys_t[subKeyNum].length = permutateTable_t.length;

}

/*
	This function expands a data block from 32 bits to 48 bits by repeating bits based off an 
	input selection (expansion) table

	INPUTS: 32bit data block, expansion table
	OUTPUTS: NONE - it modifies the input data block

	NOTE: Ensure the input array is large enough (size of expansion table) to be expanded
		- 	Also need to change the size of the input data block to the size of the expansion table
			 after running this function as this can't be done from within this function
*/
void expand32to48(array_t binaryArray_t, array_t expandTable_t){

	int i;
	char tempArray[expandTable_t.length];

	// Append the new order of bits to a temporary array
	for (i=0; i < expandTable_t.length; i++) {
		// Subtract 1 from the index since the permitation table starts at 1, not 0.
		tempArray[i] = binaryArray_t.data[(int)expandTable_t.data[i] - 1];
	}

	// Copy this temporary data back to the original array
	for (i=0; i < expandTable_t.length; i++) {
		binaryArray_t.data[i] = tempArray[i];
	}
}

/*
	This is a basic XOR function.

	INPUTS: two integers to be XOR'd
	OUTPUTS: integer result of the XOR
*/
char XOR (char a, char b){

	return (a ^ b);
}

/*
	This is an S-Box function

	INPUT: data block
	OUTPUT: NONE - it modifies the input data block
*/
void sBox (array_t binaryArray_t){

	int blockNum, i, j, count, sNum, bitNum = 0, pos = 0, k;
	/* Variable Used:
	 		blockNum - the block of 6 bits that the loop is up to
			i & j - the row and column of the SBox to use
			count - subloop counter that specifies the bit in the blockNum to copy to the 6bit block
			sNum - the decimal inside the respective sBox
			bitNum - keeps track of the position of the binaryArray_t.data to copy into the 6bit block
			pos - keeps track of the position in the binaryArray_t.data to write to
			k - used in the write-to-binary part
	*/

	char block[SBOX_BLOCK_SIZE];
	int numBlocks = binaryArray_t.length / SBOX_BLOCK_SIZE;

	for (blockNum = 0; blockNum < numBlocks; blockNum++) {

		// Add bits to block array
		for (count = 0; count < SBOX_BLOCK_SIZE; count++, bitNum++){
			block[count] = binaryArray_t.data[bitNum];
		};

		//Use first and last bits to convert to a binary number, i
		i = 0;
		if (block[0] == 1){
			i+=2;
		};
		if (block[5] == 1) {
			i+=1;
		};

		//Use middle bits and convert to a binary number, j
		j = 0;
		if (block[1] == 1){
			j+=8;
		};
		if (block[2] == 1) {
			j+=4;
		};
		if (block[3] == 1){
			j+=2;
		};
		if (block[4] == 1) {
			j+=1;
		};

		// Using the block number, find the correct SBox Table (eg, blockNum 1 correlates to SBox table 1)
		// Once found, find the number on the i'th row, and j'th column
		if (blockNum == 0) {
			sNum = S1[i*16 + j];
		} else if (blockNum == 1) {
			sNum = S2[i*16 + j];
		} else if (blockNum == 2) {
			sNum = S3[i*16 + j];
		} else if (blockNum == 3) {
			sNum = S4[i*16 + j];
		} else if (blockNum == 4) {
			sNum = S5[i*16 + j];
		} else if (blockNum == 5) {
			sNum = S6[i*16 + j];
		} else if (blockNum == 6) {
			sNum = S7[i*16 + j];
		} else if (blockNum == 7) {
			sNum = S8[i*16 + j];
		}

		// Convert sNum to a 4bit binary array
		for (count = 3; count >= 0; count--, pos++) {
			k = sNum >> count;
			if (k & 1) {
				binaryArray_t.data[pos] = 1;
			} else {
				binaryArray_t.data[pos] = 0;
			}
		}
	}
}

/*
	This is the main F function of the DES Feistel Cipher.
	It works on the entire 64bit block and handles splitting and reversing the left and right data blocks.

	INPUTS: 64bit data block, keyNumber (1-16), reverse flag (True or False)
	OUTPUTS:  NONE - it modifies the input data block
*/
void functionF (array_t dataBlock_t, int keyNum, char reverse) {
	
	int i;
	//Since these arrays are going to be expanded, they need to be the expanded size.
	char leftDataBlock[EXPANSIONTABLESIZE], rightDataBlock[EXPANSIONTABLESIZE], workingCopy[EXPANSIONTABLESIZE];

	array_t leftDataBlock_t = assignArray(EXPANSIONTABLESIZE, leftDataBlock);
	array_t rightDataBlock_t = assignArray(EXPANSIONTABLESIZE, rightDataBlock);
	//This working copy is the datablock that is modified. Can't work on the actual right data block since this needs to be
	// retained for the next time the function runs
	array_t workingCopy_t = assignArray(EXPANSIONTABLESIZE, workingCopy);

	// Copy data block into local arrays for manipulation
	for (i = 0; i < dataBlock_t.length; i++) {
		if (i < (dataBlock_t.length / 2)) {
			leftDataBlock_t.data[i] = dataBlock_t.data[i];
		} else {
			//The working copy is the same as the right data block at this point
			rightDataBlock_t.data[i - 32] = dataBlock_t.data[i];
			workingCopy_t.data[i - 32] = dataBlock_t.data[i];
		};
	};

	//Step 1 - the working copy is expanded
	expand32to48(workingCopy_t, expansionTable_t);
	workingCopy_t.length = EXPANSIONTABLESIZE;

	//Step 2 - the working copy is XORd with the subkey
	for (i=0; i < EXPANSIONTABLESIZE; i++) {
		workingCopy_t.data[i] = XOR(workingCopy_t.data[i], subKeys_t[keyNum].data[i]);
	};

	//Step 3 - the working copy is passed through an S-Box
	sBox(workingCopy_t);

	//Not nice code...but I can't think of a nice way of setting this in my S-Box function since that function only
	//  has a local copy of the variable
	workingCopy_t.length = SBOX_RESULT_LENGTH;

	//Step 4 - the working copy is passed through a permutation function
	permutate(workingCopy_t, P_t);

	//Step 5 - the working copy is XORd with the left data block
	for (i=0; i < workingCopy_t.length; i++) {
		workingCopy_t.data[i] = XOR(workingCopy_t.data[i], leftDataBlock_t.data[i]);
	};

	// This past copies the left and right data blocks back to the input data block
	// If the reverse flag is true, the right and left data blocks are copied back in reverse order
	for (i = 0; i < dataBlock_t.length; i++) {
		if (i < (dataBlock_t.length / 2)) {
			//Left side of data block
			if (reverse == TRUE) {
				dataBlock_t.data[i] = rightDataBlock_t.data[i];
			} else if (reverse == FALSE) {
				dataBlock_t.data[i] = workingCopy_t.data[i];
			} else { 
				printf("ERROR - BAD ARGUMENT IN FUNCTION F\n");
				};
		} else {
			//Right Side of data block
			if (reverse == TRUE) {
				dataBlock_t.data[i] = workingCopy_t.data[i - 32];	
			} else if (reverse == FALSE) {
				dataBlock_t.data[i] = rightDataBlock_t.data[i - 32];
			} else {
				printf("ERROR - BAD ARGUMENT IN FUNCTION F\n");
			};
		};
	};

}

/*
	This function encrypts a message using DES. It relies upon the createKeys() function having been run.

	INPUTS: 64bit data block (the message)
	OUTPUTS: NONE - it modifies the input data block
*/
void encrypt (array_t message_t) {

	int keyNum;
	// Will need to reset 64bit key and maybe the message?
	
	permutate(message_t, IP_t);

	//Correct until here//

	for (keyNum = 1; keyNum < 17; keyNum++) {
		// printf("Key %d: ", key);
		// for (i=0; i < subKeys_t[key].length; i++) {
		// 	printf("%d ", subKeys_t[key].data[i]);
		// };
		// printf(" (%d) \n", i);
		if (keyNum == 16) {
			functionF(message_t, keyNum, 0);
		} else {
			functionF(message_t, keyNum, 1);
		};
	};

	permutate(message_t, FP_t);
}

/*	
	This function decrypts a message using DES. It relies upon the createKeys() function having been run.

	INPUTS: 64bit data block (encrypted message)
	OUTPUTS: NONE - it modifies the input data block
*/

void decrypt(array_t encryptedMessage_t) {

	int keyNum;
	
	permutate(encryptedMessage_t, IP_t);

	for (keyNum = 16; keyNum > 0; keyNum--) {
		if (keyNum == 1) {
			functionF(encryptedMessage_t, keyNum, 0);
		} else {
			functionF(encryptedMessage_t, keyNum, 1);
		};
	};

	permutate(encryptedMessage_t, FP_t);
}
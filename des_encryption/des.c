/* This is my attempt at coding DES encryption 
	Compile this with gcc -Wall -Werror des.c -o des
	Created By: Adam van Zuylen in August 2020

	With guidance from:
	http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm
	https://www.techiedelight.com/des-implementation-c/
*/

//Files required
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "des.h"
#include "des_functions.c"
#include "des_tests.c"

int main (int argc, char *argv[]) {

	if (argc <= 1){

		//Load the required arrays defined in the header file
		loadArrays();

		int i;

		//A dummy 64bit key
		char key64bit[] = {
			0,0,0,1, 0,0,1,1, 0,0,1,1, 0,1,0,0, 
			0,1,0,1, 0,1,1,1, 0,1,1,1, 1,0,0,1, 
			1,0,0,1, 1,0,1,1, 1,0,1,1, 1,1,0,0, 
			1,1,0,1, 1,1,1,1, 1,1,1,1, 0,0,0,1
		};
		array_t key64bit_t = assignArray(INITIALMESSAGESIZE, key64bit);

		//A dummy message to encrypt
		char message[] = {
			0,0,0,0, 0,0,0,1, 0,0,1,0, 0,0,1,1,
			0,1,0,0, 0,1,0,1, 0,1,1,0, 0,1,1,1,
			1,0,0,0, 1,0,0,1, 1,0,1,0, 1,0,1,1,
			1,1,0,0, 1,1,0,1, 1,1,1,0, 1,1,1,1
		};
		array_t message_t = assignArray(INITIALMESSAGESIZE, message);


		printf("Message:           ");
		for (i=0; i < 64; i++) {
			printf("%d ", message_t.data[i]);
		}
		printf("\n");

		//Create the 16 keys using the pre-defined first key
		createKeys(key64bit_t);

		//Run the encryption function
		encrypt(message_t);

		printf("Encrypted Message: ");
		for (i=0; i < 64; i++) {
			printf("%d ", message_t.data[i]);
		}
		printf("\n");

		//Run the decryption function
		decrypt(message_t);
		printf("Decrypted Message: ");
		for (i=0; i < 64; i++) {
			printf("%d ", message_t.data[i]);
		}
		printf("\n");

	} else if (strcmp(argv[1], "-test") == 0){

		//Load the required arrays defined in the header file
		loadArrays();

		//Run the tests function defined in des_tests.c
		startTests();

	} else if (strcmp(argv[1], "-dev") == 0){

		printf("This area is for testing random functions\n");

	} else {

		printf("Mode not recognided\n");
	}
}
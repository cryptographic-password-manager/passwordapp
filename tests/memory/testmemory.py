from src.memory import memoryprint
from tests.testcase import ExtTestCase


class TestMemoryPrinter(ExtTestCase):

    def test_memory_empty(self):
        memory = []

        output = memoryprint(memory)

        self.assertStringContains('Memory is Empty', output)

    def test_memory_string_exists(self):
        memory = 'test string'

        output = memoryprint(memory)

        self.assertStringContains('Memory: test string', output)

    def test_memory_list_exists(self):
        memory = ['test string']

        output = memoryprint(memory)

        self.assertStringContains("Memory: test string", output)

    def test_memory_entry_too_long(self):
        memory = ['x' * 80]

        output = memoryprint(memory)

        expected = "Memory: " + 'x' * 68 + '[+12]'
        self.assertStringContains(expected, output)

    def test_memory_slightly_too_long(self):
        memory = ['x' * 73]

        output = memoryprint(memory)

        expected = "Memory: " + 'x' * 68 + '[+5]'
        self.assertStringContains(expected, output)

    def test_memory_way_too_long(self):
        memory = ['x' * 200]

        output = memoryprint(memory)

        expected = "Memory: " + 'x' * 66 + '[+134]'
        self.assertStringContains(expected, output)

    def test_multiple_entries(self):
        memory = ['x', 'x']

        output = memoryprint(memory)

        expected = "Memory: x\n        x"
        self.assertStringContains(expected, output)

    def test_max_entries(self):
        memory = ['x', 'x', 'x', 'x']

        output = memoryprint(memory)

        expected = "Memory: x\n        x\n        x\n        x"
        self.assertStringContains(expected, output)

    def test_too_many_entries(self):
        memory = ['x', 'x', 'x', 'x', 'x']

        output = memoryprint(memory)

        expected = "Memory: x\n        x\n        x\n        [+2 Rows]"
        self.assertStringContains(expected, output)

    def test_many_long_entries(self):
        memory = []
        for i in range(0, 6):
            memory.append('x' * 200)

        output = memoryprint(memory)
        expected = 'Memory: ' + 'x' * 66 + '[+134]' + '\n        ' + 'x' * 66 + '[+134]' + '\n        ' + 'x' * \
                   66 + '[+134]' + "\n        [+3 Rows]"
        self.assertStringContains(expected, output)

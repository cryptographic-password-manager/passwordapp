from unittest import TestCase


class ExtTestCase(TestCase):

    def assertStringContains(self, expected: str, actual: str):
        self.assertIn(expected, actual, "Expected: {}\nFound: {}".format(expected, actual))

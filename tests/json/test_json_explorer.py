from tests.testcase import ExtTestCase
from src.json.jsondb import Website, Websites
from src.json.json_explorer import print_website, print_websites, JsonExplorer


class TestJsonExplorer(ExtTestCase):
    def test_empty(self):
        output = print_websites([])

        self.assertEqual("", output)

    def test_single_website(self):
        output = print_websites([Website("field1", "field2", "field3").to_dict()])

        self.assertEqual("field1", output)

    def test_multiple_websites(self):
        output = print_websites([
            Website("field1", "field2", "field3").to_dict(),
            Website("field4", "field5", "field6").to_dict(),
            Website("field7", "field8", "field9").to_dict()
        ])

        self.assertEqual("field1\nfield4\nfield7", output)

    def test_website_print(self):
        website = Website("field1", "field2", "field3").to_dict()

        output = print_website(website)

        self.assertEqual("field1\nfield2\nfield3", output)

    def test_add_website(self):
        website = Website("field1", "field2", "field3")
        explorer = JsonExplorer(Websites())

        explorer.add_website(website)

        self.assertEqual(Website('field1', 'field2', 'field3'), explorer.database.list[0])

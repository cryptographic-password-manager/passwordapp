from tests.testcase import ExtTestCase
from src.ciphers.caesar import *


class TestOffsetCalculation(ExtTestCase):
    def test_no_offset(self):
        keypair = ('a', 'a')

        offset = caesarcalcoffset(keypair)

        self.assertEqual(offset, 0)

    def test_positive_offset(self):
        keypair = ('a', 'b')

        offset = caesarcalcoffset(keypair)

        self.assertEqual(offset, 1)

    def test_negative_offset(self):
        keypair = ('b', 'a')

        offset = caesarcalcoffset(keypair)

        self.assertEqual(offset, -1)

    def test_keypair_plaintext_length_error(self):
        keypair = ("string", 'a')

        with self.assertRaises(ValueError):
            caesarcalcoffset(keypair)

    def test_keypair_ciphertext_length_error(self):
        keypair = ('a', "string")

        with self.assertRaises(ValueError):
            caesarcalcoffset(keypair)


class TestCaesarSubstitute(ExtTestCase):
    def test_input_size(self):
        with self.assertRaises(ValueError):
            caesarsubsitute("string", 0)

    def test_no_offset(self):
        result = caesarsubsitute('a', 0)

        self.assertEqual(result, 'a')

    def test_positive_offset(self):
        result = caesarsubsitute('a', 1)

        self.assertEqual(result, 'b')

    def test_negative_offset(self):
        result = caesarsubsitute('b', -1)

        self.assertEqual(result, 'a')

    def test_wraparound_positive(self):
        result = caesarsubsitute('z', 1)

        self.assertEqual(result, 'a')

    def test_wraparound_negative(self):
        result = caesarsubsitute('a', -25)

        self.assertEqual(result, 'b')


class TestCaesarEncrypt(ExtTestCase):
    def test_no_offset(self):
        phrase = 'abcd'
        key = ('a', 'a')

        result = caesarencrypt(phrase, key)

        self.assertEqual('abcd', result)

    def test_positive_offset(self):
        phrase = 'abcd'
        key = ('a', 'b')

        result = caesarencrypt(phrase, key)

        self.assertEqual('bcde', result)

    def test_negative_offset(self):
        phrase = 'bcde'
        key = ('b', 'a')

        result = caesarencrypt(phrase, key)

        self.assertEqual('abcd', result)

    def test_positive_wraparound(self):
        phrase = 'xyz'
        key = ('a', 'b')

        result = caesarencrypt(phrase, key)

        self.assertEqual('yza', result)

    def test_negative_wraparound(self):
        phrase = 'abc'
        key = ('b', 'a')

        result = caesarencrypt(phrase, key)

        self.assertEqual('zab', result)

    def test_multiple_words(self):
        phrase = 'a a'
        key = ('a', 'a')

        result = caesarencrypt(phrase, key)

        self.assertEqual('a a', result)

    def test_nonalpha(self):
        phrase = '%'
        key = ('a', 'b')

        result = caesarencrypt(phrase, key)

        self.assertEqual('%', result)

    def test_multiple_sentences(self):
        phrase = ['abcd', 'abcd']
        key = ('a', 'b')

        result = caesarencrypt(phrase, key)

        self.assertEqual(['bcde', 'bcde'], result)

from tests.testcase import ExtTestCase
from src.ciphers.AES import AES
from src.ciphers import controller

cipher = AES.dummy()
plaintext = ["0"*16]
ciphertext = b'\xb0\x11\x7f\xdb%\xc9\x0c\xba\xb4e\x0e\xab\xa5\xf2\xa8D'

class TestHandler(ExtTestCase):
	def test_get_cipher_over_limit(self):
		expectedResult = "ValueError"
		
		try:
			result = AES.Get_Cipher('0'*33)
		except ValueError:
			result = "ValueError"
		
		self.assertEqual(expectedResult, result)
		
class TestEndToEnd(ExtTestCase):
	def test_encrypt_to_decrypt(self):
		testInstance = controller.CipherController
		testInstance.memory = ["testvalue"]
		
		result = controller.CipherController.encrypt_menu_logic(testInstance, '2', "testpass")
		
		testInstance.memory = result
		result = controller.CipherController.decrypt_menu_logic(testInstance, '2', "testpass")
		
		expectedResult = ["testvalue"]
		self.assertEqual(expectedResult, result)

class TestAESDecrypt(ExtTestCase):
	def test_single_decrypt(self):
		result = AES.AES_Decrypt(ciphertext, cipher)
		expectedResult = plaintext
		self.assertEqual(expectedResult,result)

	def test_double_decrypt(self):
		expectedResult = 1
		try:
			AES.AES_Decrypt(plaintext, cipher)
		except TypeError:
			result = 1
		self.assertEqual(expectedResult,result)
		
class TestAESEncrypt(ExtTestCase):
	def test_single_encrypt (self):
		result = AES.AES_Encrypt(plaintext, cipher)
		expectedResult = ciphertext
		self.assertEqual(expectedResult,result)

	def test_double_encrypt(self):
		expectedResult = 1
		try:
			AES.AES_Encrypt(ciphertext, cipher)
		except TypeError:
			result = 1
		self.assertEqual(expectedResult,result)
		
from tests.testcase import ExtTestCase
import os
import io
import sys
import os.path
from unittest import defaultTestLoader, TextTestRunner, TestSuite
from src.filehelpers import readfile, writefile
from unittest.mock import patch

FILENAME = "testfile.txt"


class TestFileOpening(ExtTestCase):
    def setUp(self):
        helperdeletefile()

    def tearDown(self):
        helperdeletefile()

    def test_file_read_singleline(self):
        helperwritefile('content\n')

        filecontent = readfile(FILENAME)

        self.assertEqual(len(filecontent), 1)
        self.assertStringContains("content", filecontent[0])

    def test_file_read(self):
        helperwritefile()

        filecontent = readfile(FILENAME)

        self.assertEqual(len(filecontent), 2)
        self.assertStringContains("test file contents", filecontent[0])
        self.assertStringContains("even has multilines", filecontent[1])

    @patch('builtins.input', lambda *args: "")
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_file_read_error(self, mock_stdout):

        with self.assertRaises(FileNotFoundError):
            readfile("notrealfile.txt")

        self.assertStringContains("File Read Error (File Not Found)", mock_stdout.getvalue())

    @patch('builtins.input', lambda *args: FILENAME)
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_user_reenters_filename(self, mock_stdout):
        helperwritefile()

        try:
            content = readfile("notrealfile.txt")
        except FileNotFoundError:
            self.fail("Should not throw an exception")

        self.assertStringContains("File Read Error (File Not Found)", mock_stdout.getvalue())
        self.assertStringContains("test file contents", content)


class TestFileWriting(ExtTestCase):
    class TestSingleLine(ExtTestCase):
        def setUp(self):
            helperdeletefile()

        def tearDown(self):
            helperdeletefile()

        content = 'some content'

        @patch('builtins.input', lambda *args: "")
        def test_file_write_singleline(self):
            writefile(FILENAME, str(self.content))

            loadedcontent = helperreadfile(FILENAME)
            self.assertStringContains(self.content, loadedcontent)

        @patch('builtins.input', lambda *args: "")
        def test_file_already_exists_error(self):
            helperwritefile()

            with self.assertRaises(FileExistsError):
                writefile(FILENAME, self.content)

        @patch('builtins.input', lambda *args: FILENAME)
        def test_user_overwrites_file(self):
            writefile(FILENAME, self.content)
            writefile(FILENAME, self.content)

            loadedcontent = helperreadfile(FILENAME)
            self.assertStringContains(self.content, loadedcontent)

        @patch('builtins.input', lambda *args: FILENAME + '1')
        def test_user_renames_file(self):
            helperwritefile()
            writefile(FILENAME, self.content)

            loadedcontent = helperreadfile(FILENAME + '1')
            self.assertStringContains(self.content, loadedcontent)

            helperdeletefile(FILENAME + '1')

    class TestMultiLine(ExtTestCase):

        def setUp(self):
            helperdeletefile()

        def tearDown(self):
            helperdeletefile()

        content = ['some content', 'secondline']

        def test_file_write_multiline(self):
            writefile(FILENAME, str(self.content))

            loadedcontent = helperreadfile(FILENAME)
            self.assertStringContains(self.content[0], loadedcontent)
            self.assertStringContains(self.content[1], loadedcontent)

        @patch('builtins.input', lambda *args: "")
        def test_file_already_exists_error(self):
            helperwritefile()

            with self.assertRaises(FileExistsError):
                writefile(FILENAME, self.content)

        @patch('builtins.input', lambda *args: FILENAME)
        def test_user_overwrites_file(self, ):
            writefile(FILENAME, self.content)
            writefile(FILENAME, self.content)

            loadedcontent = helperreadfile(FILENAME)
            self.assertStringContains(self.content[0], loadedcontent)
            self.assertStringContains(self.content[1], loadedcontent)

        @patch('builtins.input', lambda *args: FILENAME + '1')
        def test_user_renames_file(self):
            helperwritefile()

            writefile(FILENAME, self.content)

            loadedcontent = helperreadfile(FILENAME + '1')
            self.assertStringContains(self.content[0], loadedcontent)
            self.assertStringContains(self.content[1], loadedcontent)

            helperdeletefile(FILENAME + '1')

    def test_inner_test_class(self):
        testlist = []
        testsuite = defaultTestLoader.loadTestsFromTestCase(self.TestSingleLine)
        testlist.append(testsuite)
        testsuite = defaultTestLoader.loadTestsFromTestCase(self.TestMultiLine)
        testlist.append(testsuite)
        newsuite = TestSuite(testlist)
        TextTestRunner().run(newsuite)


def helperwritefile(content="test file contents\neven has multilines\n"):
    f = open(FILENAME, "w")
    f.write(content)
    f.close()


def helperreadfile(filename):
    f = open(filename, 'r')
    contents = f.read()
    f.close()
    return contents


def helperdeletefile(filename=FILENAME):
    if os.path.exists(filename):
        os.remove(filename)

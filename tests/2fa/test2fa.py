from tests.testcase import ExtTestCase
from unittest.mock import patch
from src.twofa.twofa import print_qr, check_valid
from src.exceptions import FailedMFAException
from pyqrcode import QRCode
import pyotp


class Test2FA(ExtTestCase):

    @patch('sys.stdout', return_value=None)
    @patch('pyqrcode.create', return_value=QRCode(content='somethingelse'))
    #for some reason the patch order is opposite to the signature order? Idk why this is
    def test_printing(self, mock_qr, mocked_sysout_to_stop_printing):
        secretKey = pyotp.random_base32()
        expected = pyotp.totp.TOTP(secretKey).provisioning_uri(issuer_name='passwordapp')
        print_qr(secretKey)
        mock_qr.assert_called_with(expected)

    def test_check_valid(self):
        secret = 'base32secret3232'
        totp = pyotp.TOTP(secret)
        usercode = totp.now()

        result = check_valid(secret, usercode)

        self.assertTrue(result)



    def test_check_invalid(self):
        secret = 'base32secret3232'
        totp = pyotp.TOTP(secret)
        usercode = totp.now()

        with self.assertRaises(FailedMFAException):
            check_valid('differentsecret', usercode)


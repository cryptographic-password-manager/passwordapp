# Password Management Application



## Installation

### Ubuntu/WSL

`./run install`

n.b. Do not run the setup script more than once, you'll end up with duplicated entries in your ~/.profile (not that this is 'technically' an issue, its just needless)

### OSX

`./run osxInstall`

## Running Application

`./run app`


## Running Tests

`./run test`

Alternatively `./run debug_test` runs all tests verbosely

## Running Python Manually

`./run activate` loads the venv and puts you into a venv shell with python libraries installed



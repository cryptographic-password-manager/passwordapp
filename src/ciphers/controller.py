from src.memory import memoryprint
from src.exceptions import InvalidOptionException
from src.ciphers.caesar import caesarencrypt
from src.ciphers.AES import AES_Handler

class CipherController:
    sentinel = True
    memory = []
    methods = ['unselected', 'Caesar Cipher', 'AES']
    encryptedmemory = []
    loggederror = ''

    def encrypt(self, memory):
        self.sentinel = True
        self.memory = memory
        self.encryptedmemory = memory
        while self.sentinel:
            self.print_options('En')
            try:
                self.encryptedmemory = self.encrypt_menu_logic()
            except InvalidOptionException:
                self.loggederror = 'Invalid Selection'
            except Exception:
                self.loggederror = 'System Error'

        return self.encryptedmemory

    def encrypt_menu_logic(self, selection=None, keyAES=None):
        if self.loggederror:
            print(self.loggederror)
            self.loggederror = ''
        if not selection:
            selection = input(">")

        if selection == '1':
            plaintext = input('Input plaintext character>')
            ciphertext = input('Input mapped ciphertext character>')
            keypair = (plaintext, ciphertext)
            try:
                result = caesarencrypt(self.memory, keypair)
                print('Caesar Cipher Successful')
                self.sentinel = False
                return result
            except ValueError:
                print('Invalid Keypair Input')
                
        elif selection == '2':
            result = AES_Handler(self.memory, 'e', keyAES)
            self.sentinel = False
            return result
                
        elif selection.lower() == 'e':
            self.sentinel = False
            return self.memory
        else:
            raise InvalidOptionException

    def print_options(self, methodPrefix):
        print('-' * 80)
        print(methodPrefix + 'cryption Options')
        print(memoryprint(self.memory))
        print('-' * 80)
        
        iterMethods = iter(self.methods)
        next(iterMethods)
        for x in iterMethods:
            print( str(self.methods.index(x)) + ') ' + x)
        
        print('e) Exit')

    def decrypt(self, memory):
        self.sentinel = True
        self.memory = memory
        self.encryptedmemory = memory

        while self.sentinel:
            self.print_options('De')
            try:
                self.encryptedmemory = self.decrypt_menu_logic()
            except InvalidOptionException:
                self.loggederror = 'Invalid Selection'
            except Exception:
                self.loggederror = 'System Error'

        return self.encryptedmemory

    def decrypt_menu_logic(self, selection=None, keyAES=None):
        if self.loggederror:
            print(self.loggederror)
            self.loggederror = ''
        if not selection:
            selection = input(">")

        if selection == '1':
            plaintext = input('Input ciphertext character>')
            ciphertext = input('Input mapped plaintext character>')
            keypair = (plaintext, ciphertext)
            try:
                result = caesarencrypt(self.memory, keypair)
                print('Caesar Cipher Successful')
                self.sentinel = False
                return result
            except ValueError:
                print('Invalid Keypair Input')
                
                
        elif selection == '2':
            result = AES_Handler(self.memory, 'd', keyAES)
            self.sentinel = False
            return result
        
        elif selection.lower() == 'e':
            self.sentinel = False
            return self.memory
        else:
            raise InvalidOptionException
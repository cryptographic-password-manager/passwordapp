import math
from Crypto.Cipher import AES
import getpass

def dummy():
    cipher = AES.new('0' * 32)
    return cipher


def AES_Encrypt(inputData, cipher):
    data = ",".join(inputData)
    blockSize = 16 * math.ceil(len(data) / 16)
    data = data.ljust(blockSize, ' ')

    ciphertext = cipher.encrypt(data)
    return ciphertext


def AES_Decrypt(inputData, cipher):
    ciphertext = inputData

    plaintext = cipher.decrypt(ciphertext)

    plaintext = plaintext.decode("utf-8")
    plaintext = plaintext.strip()

    listText = plaintext.split(',')
    return listText


def AES_Handler(inputData, direction, key=None):
    cipher = Get_Cipher(key)

    if direction == 'e':
        result = AES_Encrypt(inputData, cipher)
        return result

    if direction == 'd':
        result = AES_Decrypt(inputData, cipher)
        return result

    raise Exception("invalid method")


def Get_Cipher(key=None):
    if not key:
        print('')
        print('enter encryption key, Max 32 char')
        key = getpass.getpass(prompt='>', stream=None)
    key = key.ljust(32, ' ')

    cipher = AES.new(key)
    return cipher

import string


def caesarencrypt(phrase, key: tuple):
    if type(phrase) == list:
        ciphertext = []
        for sentence in phrase:
            ciphertext.append(caesarencrypt(sentence, key))
    else:
        ciphertext = ''
        offset = caesarcalcoffset(key)
        for letter in phrase:
            if letter not in list(string.ascii_lowercase):
                ciphertext = ciphertext + letter
            else:
                ciphertext = ciphertext + caesarsubsitute(letter, offset)
    return ciphertext


def caesarsubsitute(input: str, offset: int):
    if len(input) != 1:
        raise ValueError
    result = ord(input.lower()) + offset

    if result > ord('z'):
        overshoot = result - ord('z')
        result = ord('a') - 1 + overshoot
    if result < ord('a'):
        undershoot = ord('a') - result
        result = ord('z') + 1 - undershoot
    return chr(result)


def caesarcalcoffset(keypair: tuple):
    if len(keypair[0]) > 1 or len(keypair[1]) > 1:
        raise ValueError
    plaintext = ord(keypair[0])
    ciphertext = ord(keypair[1])

    result = ciphertext - plaintext
    return result

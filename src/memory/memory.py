def memoryprint(memory: list, header=True):
    if not header:
        beginning = '        '
    else:
        beginning = 'Memory: '
    if not memory and header is True:
        return "Memory is Empty"
    elif not memory and header is False:
        return ''
    elif type(memory) == list:
        output = memoryprint(memory[0])
        if len(memory) > 4:
            for entry in memory[1:3]:
                output = output + '\n' + memoryprint(entry, header=False)
            output = output + '\n        [+{} Rows]'.format(len(memory)-3)
        else:
            for entry in memory[1:]:
                output = output + '\n' + memoryprint(entry, header=False)
        return output
    elif len(memory) + len(beginning) > 80:
        overshoot = (len(memory) + len(beginning)) - 80
        brackets = 3
        toremove = overshoot + brackets + len(str(overshoot))
        return "{}{}[+{}]".format(beginning, memory[:-toremove], toremove)
    else:
        return "{}{}".format(beginning, memory)


def getuserinput():
    sentinel = True
    memory = []

    while sentinel:
        userinput = input('Input line> ')
        if not userinput:
            sentinel = False
        else:
            memory.append(userinput)
    return memory

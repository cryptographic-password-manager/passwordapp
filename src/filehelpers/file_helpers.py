import os


def readfile(filename: str):
    try:
        f = open(filename, "r")
        contents = f.read().split('\n')
        f.close()
        return contents[:-1]
    except FileNotFoundError:
        print("File Read Error (File Not Found)")
        usrinput = input("Reenter Filename (RETURN to cancel): ")
        if not usrinput:
            raise FileNotFoundError
        else:
            return readfile(usrinput)


def writefile(filename: str, content):
    if checkfileexists(filename, content):
        return
    if type(content) == str:
        writeline(filename, content)
    elif type(content) == list:
        for line in content:
            writeline(filename, line+'\n')
    else:
        raise TypeError


def writeline(filename: str, content: str):
    f = open(filename, 'a')
    f.write(content)
    f.close()


def overwritefile(filename: str):
    f = open(filename, 'w')
    f.write('')
    f.close()


def checkfileexists(filename: str, content):
    fileexists = os.path.isfile(os.path.join(os.getcwd(), filename))
    if not fileexists:
        return False
    else:
        usrinput = input("Filename in use, enter new filename, reenter to overwrite, empty to cancel: ")
        if not usrinput:
            raise FileExistsError
        elif usrinput == filename:
            overwritefile(filename)
            return False
        elif usrinput != filename:
            writefile(usrinput, content)
            return True

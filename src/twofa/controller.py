from src.memory import memoryprint
from src.exceptions import InvalidOptionException, NoMFASecretException, FailedMFAException
from .twofa import generate_secret_key, print_qr, check_valid


class TwoFAController:
    sentinel = True
    memory = []
    loggederror = ''
    secretkey = ''

    def menu(self, memory):
        self.sentinel = True
        self.memory = memory
        while self.sentinel:
            self.print_options()
            try:
                self.menu_logic()
            except InvalidOptionException:
                self.loggederror = 'Invalid Selection'
            except NoMFASecretException:
                self.loggederror = 'No secret key entered'
            except FailedMFAException:
                self.loggederror = 'Invalid TOTP'
            except Exception:
                self.loggederror = 'System Error'

    def print_options(self):
        print('-' * 80)
        print('2FA')
        print('Secret Key: {}'.format(self.secretkey))
        print(memoryprint(self.memory))
        print('-' * 80)
        print('1) Generate A 2FA Secret Key')
        print('2) Enter A 2FA Secret Key')
        print('3) Check A TOTP Code')
        print('E) Exit')

    def menu_logic(self):
        if self.loggederror:
            print(self.loggederror)
            self.loggederror = ''
        selection = input(">")

        if selection == '1':
            self.secretkey = generate_secret_key()
            print_qr(self.secretkey)
        elif selection == '2':
            self.secretkey = input('Input secret key> ')
            print_qr(self.secretkey)
        elif selection == '3':
            if self.secretkey is None or '':
                raise NoMFASecretException
            usercode = input('Enter TOTP to check> ')
            result = check_valid(self.secretkey, usercode)
            if result is True:
                print("Code is valid")
            else:
                raise RuntimeError
        elif selection.lower() == 'e':
            self.sentinel = False
        else:
            raise InvalidOptionException

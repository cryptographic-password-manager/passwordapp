import pyotp
import pyqrcode
from src.exceptions import FailedMFAException


def generate_secret_key():
    return pyotp.random_base32()


def print_qr(secretkey):
    otpkey = pyotp.totp.TOTP(secretkey).provisioning_uri(issuer_name='passwordapp')
    otpurl = pyqrcode.create(otpkey)
    print(otpurl.terminal(quiet_zone=1))


def check_valid(secretkey, usercode):
    totop = pyotp.TOTP(secretkey)
    if totop.verify(usercode) is True:
        return True
    else:
        raise FailedMFAException

from __future__ import print_function, unicode_literals

from src.json.jsondb import Website, Websites
from getpass import getpass
from PyInquirer import prompt


class JsonExplorer:

    def __init__(self, database: Websites):
        self.database = database

    def menu_loop(self):
        sentinel = True
        while sentinel:
            choice = self.print_menu()['menu']
            if choice == 'Add a site':
                self.add_website_prompt()
            if choice == 'Select a site':
                website = self.select_website_prompt()
                website = self.database.find_website(website)
                self.print_website(website)
            if choice == 'Exit':
                sentinel = False

    def print_menu(self):

        choices = ['Add a site', 'Select a site', 'Exit']
        questions = [
            {
                'type': 'rawlist',
                'name': 'menu',
                'message': 'Select an option',
                'choices': choices
            }, ]
        return prompt(questions)

    def print_websites(self):
        print(print_websites(self.database.list))

    def print_website(self, website):
        print(print_website(website))

    def add_website(self, website):
        self.database.add_website(website.to_dict())

    def check_site_exists(self, sitename: str):
        result = self.database.find_website(sitename)
        if result is not None:
            return True
        else:
            return False

    def add_website_prompt(self):
        print("Adding a new website")
        name = input("Website Name: ")
        websiteExists = self.check_site_exists(name)
        if websiteExists:
            print("Error: Site Already Exists")
            return None

        username = input("Username: ")
        password = getpass()

        website = Website(name, username, password)
        self.add_website(website)

    def select_website_prompt(self):
        choices = print_websites(self.database.list).split('\n')
        questions = [
            {
                'type': 'rawlist',
                'name': 'website',
                'message': 'Select a site',
                'choices': choices
            }, ]
        return prompt(questions)['website']


def print_website(website: Website):
    return "{}\n{}\n{}".format(website["sitename"], website["username"], website["password"])


def print_websites(websites: list):
    if len(websites) < 1:
        return ''

    output = ""
    for website in websites:
        output = output + website["sitename"] + "\n"
    return output[:-1]


def main():
    websites = Websites()
    websites.add_website(Website("google", "username", "password").to_dict())
    websites.add_website(Website("yahoo", "username", "password").to_dict())
    websites.add_website(Website("bing", "username", "password").to_dict())

    explorer = JsonExplorer(websites)
    explorer.menu_loop()


if __name__ == '__main__':
    main()

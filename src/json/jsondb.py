import json


class Website:
    def __init__(self, sitename, username, password):
        self.sitename = sitename
        self.username = username
        self.password = password

    def to_dict(self):
        return {
            "sitename": self.sitename,
            "username": self.username,
            "password": self.password
        }

    def __eq__(self, other):
        if isinstance(other, str):
            return self.sitename == other
        if isinstance(other, dict):
            return self.sitename == other["sitename"] and self.username == other["username"] and self.password == other[
                "password"]
        return self.sitename == other.sitename and self.username == other.username and self.password == other.password


class Websites:

    def __init__(self, incominglist=None):
        if incominglist is None:
            incominglist = []
        self.list = incominglist

    def add_website(self, website: Website):
        if isinstance(website, Website):
            self.list.append(website.to_dict())
        else:
            self.list.append(website)

    def find_website(self, website_name: str):
        return next((item for item in self.list if item["sitename"] == website_name), None)

    def convert_to_dict(self):
        jsonlist = []
        for website in self.list:
            jsonlist.append(website)
        return jsonlist

    def print(self):
        for entry in self.list:
            print("site {} username {} password {}".format(entry["sitename"], entry["username"], entry["password"]))

    def jsonify(self):
        return json.dumps(self.convert_to_dict())

    def load_json(self, jsonstring):
        jsonlist = json.loads(jsonstring)
        for element in jsonlist:
            website = Website(
                element["sitename"],
                element["username"],
                element["password"]
            )
            self.add_website(website)


def main():
    websites = Websites()
    websites.add_website(Website("google", "username", "password"))
    websites.add_website(Website("yahoo", "username", "password"))
    websites.add_website(Website("bing", "username", "password"))

    jsontext = websites.jsonify()
    print(websites.find_website('yahoo'))

    websites2 = Websites()
    websites2.load_json(jsontext)
    websites2.print()


if __name__ == '__main__':
    main()

class InvalidOptionException(Exception):
    pass


class FailedMFAException(Exception):
    pass


class NoMFASecretException(Exception):
    pass

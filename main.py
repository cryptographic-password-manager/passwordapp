from src import InvalidOptionException
from src.memory import memoryprint, getuserinput
from src.filehelpers import readfile, writefile
from src.ciphers import CipherController
from src.twofa import TwoFAController
from src.json import JsonExplorer, Websites

class Main:
    memory = []
    sentinel = True
    loggederror = ''
    ciphercontroller = CipherController()
    twoFAController = TwoFAController()
    json_explorer = JsonExplorer(Websites())

    def run(self):
        while self.sentinel:
            self.menu_print()
            try:
                self.menu_logic()
            except InvalidOptionException:
                self.loggederror = 'Invalid Selection'
            except FileNotFoundError:
                self.loggederror = 'Error Opening File'
            except FileExistsError:
                self.loggederror = 'Filename Already In Use'
            except Exception:
                self.loggederror = "System Error"

        return 0

    def menu_print(self):
        self.menu_title_print()
        print(memoryprint(self.memory))
        print('-' * 80)
        self.menu_options_print()
        print('-' * 80)

    @staticmethod
    def menu_title_print():
        print('-' * 80)
        print("Welcome to the Password Management Application")
        print("Main Menu")
        print('\n')

    @staticmethod
    def menu_options_print():
        print("1) Load File Into Memory")
        print("2) Input Memory")
        print("3) Encrypt Memory")
        print("4) Decrypt Memory")
        print("5) Save Memory To File")
        print("6) 2FA")
        print("7) [EXPERIMENTAL] JSON Database")
        print("E) Exit")

    def menu_logic(self, selection=None):
        if self.loggederror:
            print(self.loggederror)
            self.loggederror = ''
        if not selection:
            selection = input(">")

        if selection == '1':
            filename = input("Enter filename> ")
            self.memory = readfile(filename)
        elif selection == '2':
            self.memory = getuserinput()
        elif selection == '3':
            self.memory = self.ciphercontroller.encrypt(memory=self.memory)
        elif selection == '4':
            self.memory = self.ciphercontroller.decrypt(memory=self.memory)
        elif selection == '5':
            filename = input("Enter filename> ")
            writefile(filename, self.memory)
        elif selection == '6':
            self.twoFAController.menu(self.memory)
        elif selection == '7':
            self.json_explorer.menu_loop()
        elif selection.lower() == 'e':
            self.sentinel = False
        else:
            raise InvalidOptionException


if __name__ == '__main__':
    main = Main()
    main.run()
